# springcloud-nacos-seata
本项目是根据官方的demo做了修改，实现nacos整合seata。
官方demo地址：[https://github.com/seata/seata-samples/tree/master/springcloud-nacos-seata](https://github.com/seata/seata-samples/tree/master/springcloud-nacos-seata)。

**分布式事务组件seata的使用demo，AT模式，集成nacos、springboot、springcloud、mybatis-plus，数据库采用mysql**

## 服务器版本和依赖版本
* 服务器： 
* nacos：1.3.0 
* seata：1.2.0 
* MySQL 8
* 项目： 
* Spring Boot 2.1.7.RELEASE 
* Spring Cloud Greenwich.SR2 
* spring-cloud-alibaba-seata 2.1.0.RELEASE 
* seata-all 1.2.0 
* spring-cloud-starter-alibaba-nacos-discovery 2.1.2.RELEASE 
版本一定要严格，不然会有很多的bug，比如分组配置无法生效，实例无法找到等等。。。
版本一定要严格，不然会有很多的bug，比如分组配置无法生效，实例无法找到等等。。。
版本一定要严格，不然会有很多的bug，比如分组配置无法生效，实例无法找到等等。。。

## 1. 服务端配置

### 1.1 Nacos-server

下载nacos 1.3.0，可以参考以下连接配置

[http://www.iocoder.cn/Nacos/install/?self](http://www.iocoder.cn/Nacos/install/?self)

### 1.2 Seata-server

下载seata-server 1.3.0，

#### 1.2.1 修改conf/registry.conf 配置

注意：不同版本的seata server的配置不同

~~~java
registry {
  # file 、nacos 、eureka、redis、zk、consul、etcd3、sofa
  type = "nacos"

  nacos {
  	#注册的服务名
      application = "seata-server"
      serverAddr = "127.0.0.1:8848"
      group = "SEATA_GROUP"
      namespace = ""
      cluster = "default"
      username = ""
      password = ""
    }

}

config {
  # file、nacos 、apollo、zk、consul、etcd3、springCloudConfig
  type = "nacos"

  nacos {
      serverAddr = "127.0.0.1:8848"
      namespace = ""
      group = "SEATA_GROUP"
      username = ""
      password = ""
    }
}
~~~

#### 1.2.2 下载config.txt 添加配置
下载地址：
[https://github.com/seata/seata/tree/develop/script/config-center](https://github.com/seata/seata/tree/develop/script/config-center)

将该文件放置{seata-server}根目录下。

添加如下配置
~~~properties
service.vgroup_mapping.storage-service-group=default
service.vgroup_mapping.order-service-group=default
store.type=db
~~~

service.vgroup_mapping.${your-service-gruop}=default，中间的${your-service-gruop}为自己定义的服务组名称，服务中的application.properties文件里配置服务组名称。

demo中有两个服务，分别是storage-service和order-service，所以配置如下


#### 1.2.3 下载执行脚本
执行脚本下载地址：[https://github.com/seata/seata/tree/develop/script/config-center/nacos](https://github.com/seata/seata/tree/develop/script/config-center/nacos)

放置在{seata-server}/conf目录下。

#### 1.3 启动seata-server

**分两步，如下**

~~~shell
# 初始化seata 的nacos配置
cd conf
sh nacos-config.sh 

# 启动seata-server
cd bin
sh seata-server.sh
~~~

----------

## 2. 应用配置

### 2.1 数据库初始化

~~~SQL
-- 创建 order库、业务表、undo_log表
create database seata_order;
use seata_order;

DROP TABLE IF EXISTS `order_tbl`;
CREATE TABLE `order_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT NULL,
  `commodity_code` varchar(255) DEFAULT NULL,
  `count` int(11) DEFAULT 0,
  `money` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `undo_log`
(
  `id`            BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `branch_id`     BIGINT(20)   NOT NULL,
  `xid`           VARCHAR(100) NOT NULL,
  `context`       VARCHAR(128) NOT NULL,
  `rollback_info` LONGBLOB     NOT NULL,
  `log_status`    INT(11)      NOT NULL,
  `log_created`   DATETIME     NOT NULL,
  `log_modified`  DATETIME     NOT NULL,
  `ext`           VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_undo_log` (`xid`, `branch_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8;


-- 创建 storage库、业务表、undo_log表
create database seata_storage;
use seata_storage;

DROP TABLE IF EXISTS `storage_tbl`;
CREATE TABLE `storage_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commodity_code` varchar(255) DEFAULT NULL,
  `count` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY (`commodity_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `undo_log`
(
  `id`            BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `branch_id`     BIGINT(20)   NOT NULL,
  `xid`           VARCHAR(100) NOT NULL,
  `context`       VARCHAR(128) NOT NULL,
  `rollback_info` LONGBLOB     NOT NULL,
  `log_status`    INT(11)      NOT NULL,
  `log_created`   DATETIME     NOT NULL,
  `log_modified`  DATETIME     NOT NULL,
  `ext`           VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_undo_log` (`xid`, `branch_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8;

-- 初始化库存模拟数据
INSERT INTO seata_storage.storage_tbl (id, commodity_code, count) VALUES (1, 'product-1', 9999999);
INSERT INTO seata_storage.storage_tbl (id, commodity_code, count) VALUES (2, 'product-2', 0);
~~~

### 2.2 应用配置

见代码

几个重要的配置

1. 每个应用的resource里需要配置一个registry.conf ，demo中与seata-server里的配置相同（不同版本的seata-server的registry,conf配置会有所不同）
2. application.propeties 的各个配置项，注意spring.cloud.alibaba.seata.tx-service-group 是服务组名称，与config.txt 配置的service.vgroup_mapping.${your-service-gruop}具有对应关系

----------

## 3. 测试

1. 分布式事务成功，模拟正常下单、扣库存

   localhost:9091/order/placeOrder/commit   

2. 分布式事务失败，模拟下单成功、扣库存失败，最终同时回滚

   localhost:9091/order/placeOrder/rollback 





